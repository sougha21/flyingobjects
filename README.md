# Lab instructions
## Lab Assignments
There are **4 lab assignments**, and in order to pass the course, each lab assignment needs to be submitted **before the announced corresponding deadline**. Each lab assignment will be graded as distinction (5), pass with credit (4), pass (3) or fail (U/0). Final grade of the lab assigments will be rounded average of the 4 lab assignments in the same scale (5/4/3/U). There will be bonus questions and tasks in the lab assignments. Bonus points will be used for compensating lab grades but you cannot get more than grade 5. If you do all the exercises correctly you will get grade 5. In such case, bonus points will not be usable. 

Students can ask questions in Slack channel (See blackboard for slack link). Outside the scheduled lab sessions, the teaching assistant will be available to help with the assignments every Thursday and Friday from 15:00 to 17:00. Alternatively, the students can book a discussion session with the teaching assistant regarding the lab assignments. 

## Project
The last part of this course includes the project work that needs to be implemented and submitted individually, by each participant.

There are three different project works defined by the course responsible:
1. Future frame prediction using RNNs,
2. Final frame generation using GANs, and
3. Object detection.

It is also allowed to solve more than one project, but not required and only one project will be graded. Each project introduces the topic and tasks to be solved. The dataset is provided and visualized to get an idea of the expectations of the results.

As an alternative project work, we also encourage the students to come up with a new idea for the project work, which could be related to the master thesis. In this case, the student has to inform the course responsible in advance, who has to approve the soundness of the idea. 

Each student should pick **one of these project works** and **present the obtained results** in the dedicated student seminars (see the course schedule). Note that the source code and the corresponding notebook should also be submitted before the specified deadline. 

# Installation on your own Computer
If you have a CUDA capable Computer or Laptop you can use it for the lab exercises. Here are the instructions 
1. Install Anaconda/Conda in your computer or laptop. See https://www.anaconda.com/ for details. 
2. ```git clone git@gitlab.com:aksoyeren/flyingobjects.git```. You might need a gitlab account and add your public key to your gitbab account. For details see https://docs.gitlab.com/ee/user/ssh.html#see-if-you-have-an-existing-ssh-key-pair 
3. ```cd flyingobjects``` 
4. ```conda env create -f deeplearn.yml```
5. ```jupyter notebook```


# Installation on Computer Park
Follow the steps below to setup SSH, Login to Jupyter, and setup a virtual environment in one of the servers defined below. The lab assignments use Tensorboard for logging. See below to read more instruction on Tensorboard.



## Server Names
There are ten server machines, each has two GPU cards with 11GB RAM. You can connect to one of these machines: 

Lund, Amsterdam, Berlin, Copenhagen, Barcelona, Istanbul, London, Bermingham, Liverpool, Bristol

## SSH
The school provides various GPU servers (see above) for the students, which can be accessed through tools like CMD or Putty.

In order to access the GPU servers, the IP of your network needs to be whitelisted. It is possible to access the GPU servers within the school network. However, if you want to connect outside the school network, contact helpdesk to whitelist your IP found at whatismyip.com. 

For CMD, open CMD and type: 

ssh -L 8888:localhost:8888 -p 20022 studentID@machinename.hh.se.

The parameter -L allows us to define a server port, local ip and port are needed to access jupyter notebook. In this case, the port is 8888. Replace studentID with the school username, for example: tmpaxr21 and machinename with a server. 

- `ssh -L[desiredlocalport]:localhost:[desiredandfreedistantport] -p 20022 [studentID]@[machinename].hh.se`



## Virtual Environment and git

Simply run the following commands:
- `source activate`
- ```git clone git@gitlab.com:aksoyeren/flyingobjects.git```. You might need a gitlab account and add your public key to your gitbab account. For details see https://docs.gitlab.com/ee/user/ssh.html#see-if-you-have-an-existing-ssh-key-pair 
- ```cd flyingobjects```
- Install the environment with `conda env create -f deeplearn.yml --prefix=/data/home/your_user_name/deeplearn`
- Do not forget to run `source activate` and `conda activate deeplearn` every time you login
- `jupyter notebook --port [desiredandfreedistantport] --no-browser`

Once the jupyter notebook is running, copy and paste the URL shown on the terminal to a tab on your browser. Change the port to desiredlocalport when necessary. If it is not working, the easiest way to troubleshoot is to kill all your process and re-connect the ssh. 

## Packages
The required packages are already defined in the file deeplearn.yml. 

## Tensorboard
To use tensorboard, we need to add another port to the SSH command. Tensorboard use port 6006 (default), thus, run the following command:

ssh -L 6006:localhost:6006 8888:localhost:8888.

## Utils
To observe the load of a GPU, open a terminal and write **nvidia-smi**
* nvidia-smi

Note that you can print the GPU load continuously with the following command
* watch -n 1 nvidia-smi

## Additional Material

We also recommend to read this wiki page prepared by the IT department:
https://wiki.hh.se/ite/index.php/GPU-LAB

## Some helpful commands 
1. ```which conda``` -- shows the path to your conda executable. ```which``` is also applicable to other executables for example ```which python``` will show the path to python you are currently using. 
2. ```conda env list``` -- shows the list of conda environment(s) in the system 
3. ```cat file.txt`` -- prints the content of file.txt on the terminal
4. ```rm file.ext``` -- deletes the file.ext
5. ```rm -rf folder_name``` -- deletes everthing inside folder_name including the folder_name
6. Keyboard shortcut: ```Ctrl + C``` -- terminates a running command


